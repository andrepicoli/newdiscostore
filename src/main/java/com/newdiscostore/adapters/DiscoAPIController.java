package com.newdiscostore.adapters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.newdiscostore.ports.DiscoService;
import com.newdiscostore.repo.DiscoRepository;

@RestController
public class DiscoAPIController {

	@Autowired
	DiscoRepository repository;
	
	@Autowired
	DiscoService service;
	
}

package com.newdiscostore.ports;

import com.newdiscostore.domain.CashBack;

public interface CashBackService {

	CashBack save(String genre, String weekDay, int percent);
	
	CashBack findOneByUuid(String uuid);
	
}

package com.newdiscostore.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "cashback")
public class CashBack {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="genre")
	@NotNull
	private String genre;
	
	@Column(name="weekday")
	@NotNull
	private String weekDay;
	
	@Column(name="percent")
	@NotNull
	private int percent;


	public CashBack(Long id, @NotNull String genre, @NotNull String weekDay, @NotNull int percent) {
		this.id = id;
		this.genre = genre;
		this.weekDay = weekDay;
		this.percent = percent;
	}

	
}

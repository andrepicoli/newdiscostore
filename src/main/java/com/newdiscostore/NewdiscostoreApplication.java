package com.newdiscostore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewdiscostoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewdiscostoreApplication.class, args);
	}

}

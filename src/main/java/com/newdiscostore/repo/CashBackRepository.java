package com.newdiscostore.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.newdiscostore.domain.CashBack;

@Repository
public interface CashBackRepository extends JpaRepository<CashBack, Long> {

}

package com.newdiscostore.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.newdiscostore.domain.Disco;

@Repository
public interface DiscoRepository extends JpaRepository<Disco, Long> {

}
